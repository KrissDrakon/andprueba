import { Component, OnInit } from '@angular/core';
import { DataDbService } from '../../services/data-db.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-opinion-form',
  templateUrl: './opinion-form.component.html',
  styleUrls: ['./opinion-form.component.css']
})
export class OpinionFormComponent {

  opinionForm: FormGroup;

  /* Gestiona los el dato de envío del formulario */
  createFormGroup(): FormGroup {
    return new FormGroup({ // recibe un objeto con el campo establecido
      message: new FormControl('')
    });
  }

  /*Propiedad */
  constructor(private dbData: DataDbService){ /*Llamado al servicio*/
    this.opinionForm = this.createFormGroup();
  }

  onResetForm(): any{
    this.opinionForm.reset();
  }
  /*Envío de datos al servicio de registro */
  onSaveForm(){
    console.log('Saved!');
    const newOpinion = {
      message: 'mensaje de prueba',
      messagetwo: 'mensaje de prueba dos',
    };
    console.log(newOpinion);
       this.dbData.saveMessage(newOpinion);
  }
}
