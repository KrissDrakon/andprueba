import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Message } from '../models/message.model';

@Injectable({
  providedIn: 'root'
})
export class DataDbService {
  private opinionCollection: AngularFirestoreCollection<Message>;

  constructor(private afs: AngularFirestore) { 
    this.opinionCollection = afs.collection<Message>('opinions');
  }

  saveMessage(newOpinion: Message): void {
    this.opinionCollection.add(newOpinion);
  }
}
