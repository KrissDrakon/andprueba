import { Component, Input } from '@angular/core';
import { Other } from '../../models/other.model';

@Component({
  selector: 'app-others',
  templateUrl: './others.component.html',
  styleUrls: ['./others.component.css']
})
export class OthersComponent {
  @Input() othersItem: Other;
}
