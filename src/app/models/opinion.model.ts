export interface Opinion {
    flag: string;
    subject: string;
    body: string;
    participants: string;
}