import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BodyHeaderComponent } from './components/body-header/body-header.component';
import { BodyComponent } from './components/body/body.component';
import { OpinionComponent } from './components/opinion/opinion.component';
import { OthersComponent } from './components/others/others.component';
import { InfoComponent } from './components/info/info.component';
import { OpinionFormComponent } from './components/opinion-form/opinion-form.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { DataDbService } from './services/data-db.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyHeaderComponent,
    BodyComponent,
    OpinionComponent,
    OthersComponent,
    InfoComponent,
    OpinionFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ReactiveFormsModule
  ],
  providers: [DataDbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
