// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyAY8TXClscjoNwWLgcCB-cAYsnNqqJqvqs',
    authDomain: 'andprueba-5b8a2.firebaseapp.com',
    databaseURL: 'https://andprueba-5b8a2.firebaseio.com',
    projectId: 'andprueba-5b8a2',
    storageBucket: 'andprueba-5b8a2.appspot.com',
    messagingSenderId: '1087390242738',
    appId: '1:1087390242738:web:83a44f31f33680f00fb6ab'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
