export interface Other {
    image: string;
    title: string;
    content: string;
    moreText: string;
}