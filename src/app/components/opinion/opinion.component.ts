import { Component, Input } from '@angular/core';
import { Opinion } from '../../models/opinion.model';

@Component({
  selector: 'app-opinion',
  templateUrl: './opinion.component.html',
  styleUrls: ['./opinion.component.css']
})
export class OpinionComponent {

  /*Recibe todos los items de 'opinionNotices' que deben ser del modelo interfaz 'Opinion'*/
  @Input() opinionNotices: Opinion;

  /*Colores random para las flags de cada opinión-noticia*/
  getRandomColor2(): string{
    const chars = '0123456789ABCDEF';
    let length = 6;
    let hex = '#';
    while (length--) {hex += chars[(Math.random() * 16) | 0]};
    return hex;
  }
  getRandomColor(): string {
    const color = Math.floor(0x1000000 * Math.random()).toString(16);
    return '#' + ('000000' + color).slice(-6);
  }
}
