import { Component } from '@angular/core';
import { Opinion } from '../../models/opinion.model';
import { Other } from '../../models/other.model';


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent {

  opinionNotices: Opinion[] = [ /*Items existentes en la sección Opinión-noticias*/
    {
      flag: 'FALTA UN DÍA',
      subject: '¡Únete al Pacto por Colombia! ',
      body: 'Presidencia de la república.',
      participants: '534 colombianos participando'
    }, {
      flag: '¡ACTIVO!',
      subject: '¿Sabes qué son los datos abiertos y cómo usarlos?',
      body: 'Secretaría de mobilidad de Bogotá.',
      participants: '87 colombianos participando'
    }, {
      flag: 'CONOCE LOS RESULTADOS',
      subject: 'Los datos y visualizaciones del gobierno interesantes para su uso, aprovechamiento y toma de decisiones.',
      body: 'Ministerio de las Tecnologías de la información y las Comunicaciones.',
      participants: ''
    },
  ];

  othersItem: Other[] = [ /*Items existentes en la sección Otros*/
  {
    image: '/assets/images/Colombia.jpg',
    title: '¿Sabes qué son los datos abiertos y cómo usarlos?',
    content: 'La información que producen las entidades públicas a tu alcance.',
    moreText: '¡Me interesa!'
  },
  {
    image: '/assets/images/ColombiaCo.png',
    title: 'Conoce más sobre nuestro país',
    content: 'Colombia es hermosa y queremos que conozcas más sobre ella.',
    moreText: 'ir a Colombia.co'
  },
  {
    image: '/assets/images/Escudo.jpg',
    title: 'Este portal está pensado para ti',
    content: 'GOV.CO nace para facilitarle a los ciudadanos la interacción con el Estado.',
    moreText: 'Quiero saber como funciona'
  },
  {
    image: '/assets/images/info4.png',
    title: 'Otro item que puede ser agregado',
    content: 'Contenido del nuevo item y todo lo que corresponde.',
    moreText: 'Ver más...'
  }
  ];
}
